# Script pentru încărcarea fișierelor și directoarelor pe un server FTP

Acest script Python, `FTPUploader.py`, permite încărcarea automată a fișierelor și directoarelor de pe un sistem de fișiere local pe un server FTP. Scriptul utilizează biblioteca `ftplib` pentru a efectua operațiuni de transfer de fișiere prin FTP. Este util pentru transferul eficient și automatizat al fișierelor către un server FTP, fără a fi necesară intervenția manuală.

## Instrucțiuni de folosire:

1. **Instalare Python**: Asigură-te că ai Python instalat pe sistemul tău. Dacă nu ai Python instalat, poți descărca și instala Python de pe [site-ul oficial Python](https://www.python.org/) sau poți folosi unda dintre comenzile de mai jos.


### Ubuntu / Debian:
```bash
sudo apt install python3
```

### Fedora:
```bash
sudo dnf install python3
```

### Arch Linux:
```bash
sudo pacman -S python
```

### openSUSE:
```bash
sudo zypper install python3
```

### Gentoo:
```bash
sudo emerge --ask dev-lang/python
```

### Void Linux:
```bash
sudo xbps-install -S python3
```

### Alpine Linux:
```bash
sudo apk add python3
```

### nixOS Linux:
```bash
nix-env -i python3
```

2. **Descărcare script**: Descarcă scriptul `FTPUploader.py` pe sistemul tău. Poți clona acest repository sau poți copia conținutul scriptului și să îl salvezi într-un fișier local cu numele `FTPUploader.py`.

3. **Modificare variabile**: Deschide scriptul `FTPUploader.py` într-un editor de text și modifică următoarele variabile conform detaliilor serverului FTP și a directorului local și de pe serverul FTP:

   - `hostname`: Adresa hostului serverului FTP.
   - `username`: Numele utilizatorului pentru autentificare pe serverul FTP.
   - `password`: Parola utilizatorului pentru autentificare pe serverul FTP.
   - `local_file_path`: Calea către directorul local de pe care dorești să încarci fișierele și directoarele pe serverul FTP.
   - `remote_file_path`: Calea către directorul de pe serverul FTP unde dorești să încarci fișierele și directoarele.

4. **Rulare script**: Deschide terminalul sau linia de comandă în directorul în care ai salvat scriptul `FTPUploader.py` și rulează următoarea comandă:

   ```bash
   python FTPUploader.py
   ```

   Asigură-te că ai Python instalat și configurat corect pe sistemul tău și că ai acces la internet și la serverul FTP specificat.

5. **Urmărirea progresului**: Scriptul va începe să parcurgă recursiv directorul local specificat și va încărca fiecare fișier și director găsit în directorul corespunzător pe serverul FTP. Veți vedea mesaje în terminal care indică progresul și eventualele erori întâmpinate în timpul procesului.

6. **Finalizare transfer**: După ce scriptul a terminat de încărcat toate fișierele și directoarele, veți primi un mesaj care indică încheierea transferului și închiderea conexiunii FTP.

7. **Verificare încărcare**: Verifică directorul de pe serverul FTP pentru a te asigura că fișierele și directoarele au fost încărcate cu succes.
