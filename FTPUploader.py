import os
from ftplib import FTP

def upload_ftp_dir(hostname, username, password, local_dir, remote_dir, ftp=None):
    if ftp is None:
        ftp = FTP(hostname)
        ftp.login(username, password)
    
    for item in os.listdir(local_dir):
        local_path = os.path.join(local_dir, item)
        remote_path = os.path.join(remote_dir, item)
        
        if os.path.isfile(local_path):
            with open(local_path, 'rb') as file:
                ftp.storbinary('STOR ' + remote_path, file)
            print(f"Fișierul '{local_path}' a fost încărcat cu succes pe serverul FTP.")
        elif os.path.isdir(local_path):
            try:
                ftp.mkd(remote_path)
                print(f"Directorul '{remote_path}' a fost creat cu succes pe serverul FTP.")
            except Exception as e:
                print(f"Eroare la crearea directorului '{remote_path}':", e)
            
            upload_ftp_dir(hostname, username, password, local_path, remote_path, ftp)
    
    if local_dir == '.':
        ftp.quit()
        print("Transferul s-a încheiat.")

# Exemplu de utilizare
hostname = 'adresa_ftp_server'
username = 'nume_utilizator'
password = 'parola_utilizator'
local_file_path = r'/calea/catre/directorul/fisierelor'
remote_file_path = '/cale/catre/director/pe/ftp'

# Apelul funcției upload_ftp_dir
upload_ftp_dir(hostname, username, password, local_file_path, remote_file_path)

